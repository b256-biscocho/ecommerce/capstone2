const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController.js");
const auth = require("../auth.js");


// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

});


//Route for creating an order (Non-admin only)
router.post("/order", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		order: req.body
	} 
	if(data.isAdmin) {
		res.send(false);
	} else {

	userController.createOrder(data).then(resultFromController => res.send(resultFromController))};

});

// Route to  retrieve all user
router.get("/allUsers", (req, res) => {

	userController.allUsers().then(resultFromController => res.send(resultFromController));
});



//Route for getting non-admin users
router.get("/nonadmin", (req,res)=>{
	userController.getNonAdmin().then(resultFromController => res.send(resultFromController));
});


// Route to set user as Admin

router.put("/makeadmin/:userId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		userController.makeAdmin(data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false)
	}

});




module.exports = router;