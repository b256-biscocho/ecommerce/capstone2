const jwt = require("jsonwebtoken");

const secret = "EcommerceAPI";


module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

		return jwt.sign(data, secret, {});
};

// Token Verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		console.log(token);

		token = token.slice(7, token.length);

		// verify the token using the verify() method
		return jwt.verify(token, secret, (err, data) =>  {

			// If JWT is not valid
			if(err) {

				return res.send({auth: "failed"})

			// if JWT is valid
			} else {

				// proceed to the next action 
				next();
			}
		})

	//  Token received is undefined
	} else {

		return res.send({auth: "failed"})
	}
};

// Token Decryption

module.exports.decode = (token) => {

	// if token is not undefined
	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null;

			} else {

				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {

		return null;

	}
};


