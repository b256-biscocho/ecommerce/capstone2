const User = require("../Models/User.js");
const Product = require("../Models/Product.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User Registration

module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		// User registration failed
		if(err) {

			return false;

		// User registration successful
		} else {

			return true;

		}
	})
} 

// User authentication

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {


		if(result == null) {

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				return false;
			};
		};
	});
};





//Creating an Order (non-admin only) 

module.exports.createOrder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderedProduct.push({
			products : [
				{
					productId : data.order.productId,
					productName : data.order.productName,
					quantity : data.order.quantity
				}
			],
			totalAmount : data.order.totalAmount
		});

		return user.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.order.productId).then(product => {

		product.userOrders.push({userId: data.userId});

		return product.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}

// Get all users
module.exports.allUsers = () => {

	return User.find({}).then(result => {

		return result;
	})
}



//Getting non-admin users
module.exports.getNonAdmin = () =>{
	return User.find({isAdmin: false}).then(result => {
		return result;
	})
}

//Set as admin


module.exports.makeAdmin = (reqParams) => {

	let isActiveUpdated = {
		isAdmin: true
	}

	// 
	return User.findByIdAndUpdate(reqParams.userId, isActiveUpdated).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}


