const Product = require("../Models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.addProduct = (product) => {
	
	let newProduct = new Product({
		name: product.name,
		description: product.description,
		price:product.price
	});

	return newProduct.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
			
		}
	})
};


//Retrieving all active products
module.exports.getActiveProducts = () =>{
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}




//Retrieving a specific product

module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams.productId).then(result => {
		return result;
})
}




//Updating product info(admin only)
module.exports.updateProduct = (product, paramsId) => {
	//specify the field/properties that will be updated
	let updatedProduct = {
		name: product.name,
		description: product.description,
		price: product.price
	}
	
	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err)=>{ 
		if(err) {
			return false;
		} else {
			return true;
	}
})
}





module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		//  not archived
		if (error) {

			return false;

		//  archived successfully
		} else {

			return true;

		}

	});
};





