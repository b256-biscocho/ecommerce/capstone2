// Dependencies
const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js")

// Server
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-biscocho.lbp0f9x.mongodb.net/B256_EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Routes
app.use("/users", userRoutes)
app.use("/products", productRoutes)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Connected to the cloud database`));

// Server listening
app.listen(3001, () => console.log(`API is now online on port 3001`));
